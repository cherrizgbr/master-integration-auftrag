# master-integration-auftrag #

Dieses Repository ist Teil des Prototyps einer Masterarbeit zum Thema:  
*"Konzeption und prototypische Umsetzung der Integration fachlich getriebener Prozessmodellierung in die Softwareentwicklung unter Verwendung der Business Process Model and Notation 2.0"*  

### Informationen zum Modul ###

Dieses Modul beinhaltet einen Integrationsservice, der das Lesen von Aufträgen kapselt. Der Service wird durch den Java Client verwendet. Dieser wird dadurch vom Auftragsservice entkoppelt.

### Weiterführende Übersicht ###

Der Service ist Teil der im Architekturschaubild dargestellten Integrationsschicht. Weiterführende Informationen, Zugang zur Arbeit sowie den anderen Modulen des Prototyps finden Sie auf einer eigenen [Übersichtsseite](www.cherriz.de/master) ([cherriz.de/master](www.cherriz.de/master)).  
![Architekturschaubild Prototyp](http://www.cherriz.de/master/images/content/architektur.png "Architekturschaubild Prototyp")

### Kontakt ###
[www.cherriz.de](www.cherriz.de)  
[www.cherriz.de/master](www.cherriz.de/master)  
[master@cherriz.de](mailto:master@cherriz.de)  


